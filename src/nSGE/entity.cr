abstract class SGE::Entity < SF::Sprite
  module Implementation
    getter zorder : UInt8

    def initialize
      @zorder = 0_u8
      super
    end

    def initialize(tileset : TileSet, tile_index : Int32 = tile_index)
      @zorder = 0_u8
      super(tileset.texture)
      self.texture_rect = tileset.tile_texture_rect(tile_index)
      self.origin = center
    end

    def center
      {self.texture_rect.width / 2, self.texture_rect.height / 2}
    end

    def zorder=(int)
      @zorder = int.to_u8
    end

    def tile_index
      0
    end
  end

  include Implementation
end
