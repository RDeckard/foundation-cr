abstract class SGE::Window
  COOLDOWN = 0.02
  @@instance : SGE::Window?

  def self.instance=(window : SGE::Window)
    @@instance = window
  end

  def self.instance
    @@instance.not_nil!
  end

  private module Implementation
    getter sfml_win : SF::RenderWindow
    forward_missing_to @sfml_win

    private def initialize(width, height)
      @sfml_win = SF::RenderWindow.new(
        SF::VideoMode.new(width, height),
        "SGE",
        settings: SF::ContextSettings.new(depth: 24, antialiasing: 8))
      sfml_win.vertical_sync_enabled = true
    end

    def start
      sfml_win.active = false
      drawing_needed!
      thread = spawn { rendering }

      while sfml_win.open?
        while event = sfml_win.poll_event
          case event
          when SF::Event::Closed
            stop
          when SF::Event::KeyPressed
            case event.code
            when SF::Keyboard::Escape
              stop
            else
              gen_update(event)
              drawing_needed!
            end
          when SF::Event::MouseButtonPressed
            gen_update(event)
            drawing_needed!
          when SF::Event::Resized
            gen_update(event)
            drawing_needed!
          end
        end
        sleep COOLDOWN
      end
    end

    def stop
      sfml_win.close
    end

    def gen_update(input)
      update(input)
    end

    def gen_draw
      sfml_win.clear
      draw
      sfml_win.display
    end

    def rendering
      while sfml_win.open?
        if drawing_needed?
          gen_draw
          drawn!
        end
        sleep COOLDOWN
      end
    end

    def drawing_needed?
      @drawing_needed
    end

    def drawn!
      synchronize { @drawing_needed = false }
    end

    def drawing_needed!
      synchronize { @drawing_needed = true }
    end

    private def synchronize(&block)
      (@mutex ||= Mutex.new).synchronize(&block)
    end
  end

  include Implementation

  macro inherited
    def self.start
      SGE::Window.instance = new
      SGE::Window.instance.try(&.start)
    end

    def self.stop
      SGE::Window.instance.try(&.stop)
    end
  end

  abstract def update(input)
  abstract def draw
end
