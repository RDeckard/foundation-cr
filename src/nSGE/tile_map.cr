class SGE::TileMap
  include SF::Drawable

  def initialize(@tileset : TileSet)
    @vertices = SF::VertexArray.new(SF::Quads)
  end

  def populate(tiles_indexes_table : Array(Array(Int32?)))
    @vertices = SF::VertexArray.new(SF::Quads)
    # populate the vertex array, with one quad per tile
    tiles_indexes_table.each_with_index do |tiles_indexes_row, y|
      tiles_indexes_row.each_with_index do |tile_index, x|
        if tile_index
          # define its 4 corners and texture coordinates
          @tileset.generate_vertices(
            for: tile_index,
            at: SF.vector2(x, y)
          ).each do |vertex|
            @vertices.append vertex
          end
        end
      end
    end
  end

  def draw(target, states)
    states.texture = @tileset.texture
    target.draw(@vertices, states)
  end
end
