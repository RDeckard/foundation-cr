class SGE::Map
  alias EntityTable = Array(Array(Entity?))
  getter entities_table : EntityTable
  getter tile_map : SGE::TileMap
  forward_missing_to @entities_table

  def initialize(width, height, tileset)
    @entities_table = EntityTable.new(height) {
      Array(Entity?).new(width) { nil }
    }
    @tile_map = SGE::TileMap.new(tileset)
  end

  def get(coords)
    get(coords.x, coords.y)
  end

  def get(x, y)
    entities_table[y][x]
  end

  def set(coords, entity)
    set(coords.x, coords.y, entity)
  end

  def set(x, y, entity)
    entities_table[y][x] = entity
  end

  def generate_tile_map
    @tile_map.populate(tiles_indexes_table)
  end

  def tiles_indexes_table
    entities_table.map { |row|
      row.map { |entity|
        entity && entity.tile_index
      }
    }
  end
end
