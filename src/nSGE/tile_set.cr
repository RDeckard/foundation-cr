class SGE::TileSet
  getter texture
  getter tile_size : SF::Vector2i
  getter tiles_per_row : Int32
  forward_missing_to @texture

  def initialize(tileset_path, @tile_size)
    @texture = SF::Texture.from_file(tileset_path)
    @tiles_per_row = size.x / tile_size.x
  end

  def generate_vertices(for tile_index : Int32, at position : SF::Vector2i)
    { {0, 0}, {1, 0}, {1, 1}, {0, 1} }.map do |delta|
      SF::Vertex.new(
        (position + delta) * tile_size,
        tex_coords: tile_tex_coords(tile_index, delta)
      )
    end
  end

  def tile_texture_rect(tile_index : Int32)
    SF::IntRect.new(tile_tex_coords(tile_index), tile_size)
  end

  private def tile_tex_coords(tile_index : Int32, delta : {Int32, Int32} = {0, 0})
    tile_position =
      SF.vector2(
        tile_index % tiles_per_row,
        tile_index / tiles_per_row
      )
    (tile_position + delta) * tile_size
  end
end
