class Game < SGE::Window
  WIDTH          = 80
  HEIGHT         = 50
  CHARACTER_SIZE = 20

  property scene

  def initialize
    super WIDTH, HEIGHT, CHARACTER_SIZE
  end

  def start
    @scene = DemoScene.new(self)
    super
  end

  def update(input)
    scene.try(&.update(input))
  end

  def draw
    scene.try(&.draw)
  end
end
