class Planet < SGE::Entity
  def initialize(char = ['o', 'O'].sample,
                 color = SF::Color::Green,
                 style = SF::Text::Regular)
    super
    self.zorder = 1
  end
end
