class Ship < SGE::Entity
  include SGE::Movable

  def initialize(char = 'A',
                 color = SF::Color::Red,
                 style = SF::Text::Bold)
    super
    self.zorder = 2
  end
end
