class Selector < SGE::Entity
  include SGE::Movable

  def initialize(char = ' ')
    super
    self.background_color = SF::Color::White
  end
end
