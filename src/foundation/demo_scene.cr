class DemoScene
  MAP_SIZE = 500

  getter game : Game
  getter info_pane
  getter map
  getter ship
  getter selector
  getter camera

  def initialize(@game)
    border = 5

    # INFO PANE
    @info_pane =
      SGE::Pane.new(
        border, 0,
        Game::WIDTH - border, border)
    init_info_pane(border)

    # GAME PANE WITH MAP AND CAMERA
    game_pane =
      SGE::Pane.new(
        border, border,
        Game::WIDTH - border * 2, Game::HEIGHT - border * 2)
    @map = SGE::Map.new(MAP_SIZE, MAP_SIZE)
    init_map_planets
    @camera = SGE::Camera.new(map, game_pane)

    # SHIP
    @ship = Ship.new
    ship.coords = {4, 4}
    map.add ship

    # SELECTOR
    @selector = Selector.new
    map.add selector
  end

  def update(event)
    map.remove ship
    map.remove selector
    input_handling(event)
    map.add ship
    map.add selector
    info_pane.entities[1].string =
      "Selector spot on the map: #{map.at(selector.coords).sort_by(&.zorder).map(&.class)}"
    # "Cam coords: {#{camera.coords.x}, #{camera.coords.y}}; " \
    # "Cam center: {#{camera.center.x}, #{camera.center.y}}"
    info_pane.entities[2].string = "Ship coords: {#{ship.coords.x}, #{ship.coords.y}}"
    info_pane.entities[3].string = "Selector coords: {#{selector.coords.x}, #{selector.coords.y}}"
  end

  def input_handling(event)
    case event
    when SF::Event::KeyPressed
      case event.code
      when SF::Keyboard::K
        selector.go_up_inside(map)
      when SF::Keyboard::J
        selector.go_down_inside(map)
      when SF::Keyboard::H
        selector.go_left_inside(map)
      when SF::Keyboard::L
        selector.go_right_inside(map)
      when SF::Keyboard::Z
        camera.go_up_inside
      when SF::Keyboard::S
        camera.go_down_inside
      when SF::Keyboard::Q
        camera.go_left_inside
      when SF::Keyboard::D
        camera.go_right_inside
      when SF::Keyboard::Numpad8
        ship.go_up_inside(map)
      when SF::Keyboard::Numpad2
        ship.go_down_inside(map)
      when SF::Keyboard::Numpad4
        ship.go_left_inside(map)
      when SF::Keyboard::Numpad6
        ship.go_right_inside(map)
      when SF::Keyboard::Numpad7
        ship.go_up_inside(map)
        ship.go_left_inside(map)
      when SF::Keyboard::Numpad9
        ship.go_up_inside(map)
        ship.go_right_inside(map)
      when SF::Keyboard::Numpad1
        ship.go_down_inside(map)
        ship.go_left_inside(map)
      when SF::Keyboard::Numpad3
        ship.go_down_inside(map)
        ship.go_right_inside(map)
      when SF::Keyboard::Numpad5
        ship.rotate 10
      when SF::Keyboard::Add
        camera.zoom
      when SF::Keyboard::Subtract
        camera.zoom :out
      end
    when SF::Event::MouseButtonPressed
      case event.button
      when SF::Mouse::Left
        if click_coords = camera.get_coords_of_event(event)
          selector.coords = click_coords
        end
      end
    end
  end

  def draw
    info_pane.draw
    camera.shoot
  end

  private def init_info_pane(border)
    info_pane << SF::Text.new("foundation", Game::FONT, game.character_size)
    3.times do |i|
      info = SF::Text.new("", Game::FONT, game.character_size)
      info.position = {0, Game::CHARACTER_SIZE * (i + 1)}
      info_pane << info
    end
  end

  private def init_map_planets
    (MAP_SIZE ** 2 / 10).times do
      planet = Planet.new
      loop do
        planet.coords = {rand(MAP_SIZE), rand(MAP_SIZE)}
        break unless map.at(planet.coords).any?(&.is_a?(Planet))
      end
      planet.color =
        [
          SF::Color::Green,
          SF::Color::Cyan,
          SF::Color::Yellow,
          SF::Color::Magenta,
        ].sample
      planet.style = SF::Text::Bold if rand(2).zero?
      planet.style = [planet.style, SF::Text::Italic] if rand(2).zero?
      planet.rotation = rand(360)
      map.add planet
    end
  end
end
