require "./movable"
require "./utils"

class SGE::Camera
  include Movable
  include Utils

  property map : SGE::Map
  property coords : SF::Vector2i
  getter pane : SGE::Pane
  set_vector_as_tuple :coords, :center

  def initialize(@map, @pane)
    @coords = SF::Vector2i.new(0, 0)
    raise "camera outbound the map's limits" unless inside?
  end

  def center
    coords + pane.size / 2
  end

  def center=(point : SF::Vector2i)
    temp_coords = point - pane.size / 2
    return if temp_coords.x < 0 || temp_coords.y < 0
    if map.rect.contains?(temp_coords)
      @coords = temp_coords
    else
      @coords = map.size - pane.size
    end
  end

  def inside?(other_rect : SF::IntRect? = nil)
    rect = SF::IntRect.new(coords, pane.size)
    rect.intersects?(other_rect || map.rect) == rect
  end

  def shoot
    all_visible_coords.each do |point_on_map|
      if map.rect.contains?(point_on_map)
        map.at(point_on_map).entities.sort_by(&.zorder).each do |entity|
          entity.draw_in pane, at: entity.coords - coords
        end
      end
    end
  end

  def get_coords_of_event(event : SF::Event::MouseButtonEvent)
    if (e_coords = pane.get_coords_of_event(event))
      e_coords + coords
    end
  end

  def zoom(zoom : Symbol = :in)
    return unless {:in, :out}.includes?(zoom)
    old_center = center
    pane.zoom(zoom, map.size)
    self.center = old_center
  end

  private def all_visible_coords
    ((coords.x)..(coords.x + pane.size.x - 1)).map do |x|
      ((coords.y)..(coords.y + pane.size.y - 1)).map do |y|
        SF::Vector2i.new(x, y)
      end
    end.flatten
  end
end
