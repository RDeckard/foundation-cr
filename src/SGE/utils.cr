module Utils
  macro included
    macro set_vector_as_tuple(*properties_names)
      \{% for property_name in properties_names %}
        def \{{property_name.id}}=(tuple : Tuple(Int32, Int32))
          self.\{{property_name.id}} = SF::Vector2i.new(tuple[0], tuple[1])
        end
      \{% end %}
    end

    macro coords_and_size_properties(from rect, as properties_names = {:coords, :size})
      def \{{properties_names[0].id}}
        SF::Vector2i.new(\{{rect.id}}.left, \{{rect.id}}.top)
      end

      def \{{properties_names[0].id}}=(vector : SF::Vector2i)
        self.\{{rect.id}} = SF::IntRect.new(vector, size)
      end

      def \{{properties_names[0].id}}=(tuple : Tuple(Int32, Int32))
        self.\{{rect.id}} = SF::IntRect.new(tuple[0], tuple[1], \{{rect.id}}.width, \{{rect.id}}.height)
      end

      def \{{properties_names[1].id}}
        SF::Vector2i.new(\{{rect.id}}.width, \{{rect.id}}.height)
      end

      def \{{properties_names[1].id}}=(vector : SF::Vector2i)
        self.\{{rect.id}} = SF::IntRect.new(coords, vector)
      end

      def \{{properties_names[1].id}}=(tuple : Tuple(Int32, Int32))
        self.\{{rect.id}} = SF::IntRect.new(\{{rect.id}}.left, \{{rect.id}}.top, tuple[0], tuple[1])
      end
    end
  end
end
