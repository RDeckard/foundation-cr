class SGE::Matrix(T)
  include Utils

  getter rect : SF::IntRect
  coords_and_size_properties from: :rect
  property value : Array(Array(T))
  forward_missing_to @value

  def initialize(size : SF::Vector2i, item : T? = nil)
    @rect = SF::IntRect.new(SF::Vector2i.new(0, 0), size)
    @value = init_all(item)
  end

  def init_all!(item : T)
    self.value = init_all(item)
  end

  def at(coords)
    at(coords.x, coords.y)
  end

  def at(x, y)
    value[y][x]
  end

  def printable_string
    value.map do |row|
      row.map(&.to_s).join(' ')
    end.join("\n")
  end

  def clone
    clone = Matrix(T).new(size)
    clone.value = value.clone
    clone
  end

  private def init_all(item : T? = nil)
    Array(Array(T)).new(size.y) {
      Array(T).new(size.x) {
        item ? item.clone : T.new
      }
    }
  end
end
