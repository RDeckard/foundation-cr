require "./utils"

abstract class SGE::Entity
  private module Implementation
    include Utils

    property coords : SF::Vector2i
    getter zorder : UInt8
    property background_color : SF::Color
    property sfml_text : SF::Text
    forward_missing_to @sfml_text
    set_vector_as_tuple :coords

    def initialize(char = ' ', color = Window::DEFAULT_COLOR, style = SF::Text::Regular)
      @coords = SF::Vector2i.new(0, 0)
      @zorder = 0_u8
      @background_color = SF::Color::Transparent
      @sfml_text = SF::Text.new(char.to_s, Window::FONT)
      sfml_text.color = color
      self.style = style
    end

    def char : Char
      sfml_text.string.chars.first
    end

    def char=(char : Char)
      sfml_text.string = char.to_s
    end

    def zorder=(int)
      @zorder = int.to_u8
    end

    def style=(styles : SF::Text::Style | UInt32 | Array(SF::Text::Style | UInt32))
      sfml_text.style =
        SF::Text::Style.new(
          [styles]
            .flatten
            .reduce(SF::Text::Regular) { |acc, style|
            acc.to_i | style.to_i
          }.to_i
        )
    end
  end

  def inside?(rect : SF::IntRect)
    rect.contains? coords
  end

  def draw_in(pane : SGE::Pane, at corrected_coords = nil)
    self.character_size = pane.character_size
    self.origin = pane.char_origin
    self.position =
      (corrected_coords || coords) *
        character_size + origin + pane.px_offset + # px conversion on pane
        pane.position                              # px conversion on window

    background =
      SF::RectangleShape.new({character_size, character_size})
    background.position = position - origin - pane.px_offset
    background.fill_color = background_color
    pane.window.sfml_win.draw background
    pane.window.sfml_win.draw sfml_text
  end

  include Implementation

  macro inherited
    def clone
      clone = {{@type.name.id}}.new
      clone.char = char
      clone.coords = coords
      clone.zorder = zorder
      clone.background_color = background_color
      clone.sfml_text = sfml_text
      clone
    end
  end
end
