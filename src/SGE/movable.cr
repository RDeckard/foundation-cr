module SGE::Movable
  def go_up
    self.coords -= {0, 1}
  end

  def go_down
    self.coords += {0, 1}
  end

  def go_left
    self.coords -= {1, 0}
  end

  def go_right
    self.coords += {1, 0}
  end

  def go_up_inside(object = nil)
    go_up
    go_down unless inside?(get_rect(object))
  end

  def go_down_inside(object = nil)
    go_down
    go_up unless inside?(get_rect(object))
  end

  def go_left_inside(object = nil)
    go_left
    go_right unless inside?(get_rect(object))
  end

  def go_right_inside(object = nil)
    go_right
    go_left unless inside?(get_rect(object))
  end

  private def get_rect(object)
    case object
    when SF::Vector2i, Nil
      object
    else
      object.rect
    end
  end
end
