class SGE::Map
  getter empty_matrix : Matrix(Spot)
  getter matrix : Matrix(Spot)
  forward_missing_to @matrix

  def initialize(width, height)
    @empty_matrix = Matrix(Spot).new(SF::Vector2i.new(width, height))
    @matrix = empty_matrix.clone
  end

  def clear!
    @matrix = empty_matrix.clone
  end

  def add(entity)
    matrix[entity.coords.y][entity.coords.x] << entity
  end

  def remove(entity)
    matrix[entity.coords.y][entity.coords.x].delete(entity)
  end

  private record Spot, entities = [] of Entity do
    forward_missing_to @entities

    def to_s
      entities.map(&.char)
    end
  end
end
