class SGE::Pane
  include Utils

  property rect : SF::IntRect
  coords_and_size_properties from: :rect
  getter character_size : Int32
  getter char_origin : SF::Vector2i
  getter px_rect : SF::IntRect
  coords_and_size_properties from: :px_rect, as: {:position, :px_size}
  getter px_offset : SF::Vector2i
  property entities = [] of SGE::Entity | SF::Text
  forward_missing_to @entities

  def initialize(x = 0, y = 0, width = window.rect.width, height = window.rect.height)
    @rect = SF::IntRect.new(x, y, width, height)
    @character_size = window.character_size
    @char_origin = compute_char_origin
    @px_rect = SF::IntRect.new(
      coords * character_size,
      size * character_size)
    @px_offset = compute_px_offset
    raise "outbound window's limits" unless px_rect.intersects?(window.px_rect) == px_rect
  end

  def draw
    entities.each { |entity| draw(entity.as(SF::Drawable)) }
  end

  def draw(drawable : SF::Drawable)
    drawable.character_size = character_size
    drawable.origin = char_origin
    origin_position = drawable.position
    drawable.position += position + char_origin
    window.sfml_win.draw drawable
    drawable.position = origin_position
  end

  def zoom(zoom : Symbol = :in, area_size : SF::Vector2i? = nil)
    return unless {:in, :out}.includes?(zoom)
    int = (zoom == :in ? 2 : -2)
    zoom_by_size =
      Proc(Int32, Tuple(SF::Vector2i, Int32)).new { |int|
        temp_size = size - {int, int}
        temp_character_size =
          [px_size.x, px_size.y].min / [temp_size.x, temp_size.y].min
        {px_size / temp_character_size, temp_character_size}
      }
    zoom_by_character_size =
      Proc(Int32, Tuple(SF::Vector2i, Int32)).new { |int|
        temp_character_size = character_size + int
        {px_size / temp_character_size, temp_character_size}
      }

    if int > 0
      new_size, new_character_size = zoom_by_size.call(int)
      if new_character_size == character_size
        new_size, new_character_size = zoom_by_character_size.call(int)
      end
    else
      new_size, new_character_size = zoom_by_character_size.call(int)
      if new_size == size
        new_size, new_character_size = zoom_by_size.call(int)
      end
    end

    if new_character_size > 3 &&
       !new_size.any? { |a| a < 3 } &&
       (
         area_size.nil? ||
         new_size.x < area_size.x &&
         new_size.y < area_size.y
       )
      @character_size = new_character_size
      @char_origin = compute_char_origin
      @px_offset = compute_px_offset
      self.size = new_size
    end
  end

  def get_coords_of_event(event : SF::Event::MouseButtonEvent)
    e_coords = SF::Vector2i.new(event.x, event.y)
    (e_coords - position) / character_size if px_rect.contains?(e_coords)
  end

  def window
    SGE::Window.instance
  end

  def compute_char_origin
    SF::Vector2i.new(
      (character_size * 0.30).to_i,
      (character_size * 0.72).to_i
    )
  end

  def compute_px_offset
    SF::Vector2i.new(
      (character_size * 0.20).to_i,
      -(character_size * 0.20).to_i
    )
  end
end
