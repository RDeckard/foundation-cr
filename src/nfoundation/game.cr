class Game < SGE::Window
  WIDTH  = 1024
  HEIGHT =  768

  enum Direction
    Up
    Down
    Left
    Right
  end

  property scene

  def initialize
    super WIDTH, HEIGHT
  end

  def start
    @scene = DemoScene.new(self)
    super
  end

  def update(input)
    scene.try(&.update(input))
  end

  def draw
    scene.try(&.draw)
  end
end
