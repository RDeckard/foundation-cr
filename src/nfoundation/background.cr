class Background < SGE::Entity
  def initialize
    super
    self.texture = SF::Texture.from_file("resources/space.png")
    self.scale = {
      Game::WIDTH.to_f / texture_rect.width,
      Game::HEIGHT.to_f / texture_rect.height,
    }
  end
end
