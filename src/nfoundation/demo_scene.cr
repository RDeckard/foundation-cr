class DemoScene
  TILE_SIZE  =  300
  MAP_WIDTH  = 1000
  MAP_HEIGHT =  500

  alias Direction = Game::Direction

  getter game : Game
  getter ship : Ship
  getter selector : Selector
  getter background : Background
  getter view
  getter map

  def initialize(@game)
    planets_tileset =
      SGE::TileSet.new(
        "resources/planets.png",
        tile_size: SF.vector2(TILE_SIZE, TILE_SIZE))
    ships_tileset =
      SGE::TileSet.new(
        "resources/ships.png",
        tile_size: SF.vector2(TILE_SIZE, TILE_SIZE))

    @map = SGE::Map.new(MAP_WIDTH, MAP_HEIGHT, planets_tileset)
    init_map_planets

    @ship = Ship.new(ships_tileset)
    ship.position = SF.vector2(8 * TILE_SIZE + TILE_SIZE / 2, 4 * TILE_SIZE + TILE_SIZE / 2)

    @selector = Selector.new
    selector.position = SF.vector2(8 * TILE_SIZE + TILE_SIZE / 2, 4 * TILE_SIZE + TILE_SIZE / 2)

    @background = Background.new

    @view = View.new(
      center: SF.vector2(8 * TILE_SIZE, 4 * TILE_SIZE),
      size: SF.vector2(8 * TILE_SIZE, 6 * TILE_SIZE))
  end

  def update(event)
    input_handling(event)
  end

  def input_handling(event)
    case event
    when SF::Event::KeyPressed
      case event.code
      when SF::Keyboard::K
        selector.move Direction::Up
      when SF::Keyboard::J
        selector.move Direction::Down
      when SF::Keyboard::H
        selector.move Direction::Left
      when SF::Keyboard::L
        selector.move Direction::Right
      when SF::Keyboard::Z
        view.move Direction::Up
      when SF::Keyboard::S
        view.move Direction::Down
      when SF::Keyboard::Q
        view.move Direction::Left
      when SF::Keyboard::D
        view.move Direction::Right
      when SF::Keyboard::Numpad8
        ship.move Direction::Up
      when SF::Keyboard::Numpad2
        ship.move Direction::Down
      when SF::Keyboard::Numpad4
        ship.move Direction::Left
      when SF::Keyboard::Numpad6
        ship.move Direction::Right
      when SF::Keyboard::Numpad7
        ship.move Direction::Up
        ship.move Direction::Left
      when SF::Keyboard::Numpad9
        ship.move Direction::Up
        ship.move Direction::Right
      when SF::Keyboard::Numpad1
        ship.move Direction::Down
        ship.move Direction::Left
      when SF::Keyboard::Numpad3
        ship.move Direction::Down
        ship.move Direction::Right
      when SF::Keyboard::Numpad5
        ship.rotate 5
      when SF::Keyboard::Add
        view.zoom_in
      when SF::Keyboard::Subtract
        view.zoom_out
      end
    when SF::Event::MouseButtonPressed
      case event.button
      when SF::Mouse::Left
        world_pos =
          game.map_pixel_to_coords({event.x, event.y})
        selector.position = {
          world_pos.x.to_i / TILE_SIZE * TILE_SIZE + TILE_SIZE / 2,
          world_pos.y.to_i / TILE_SIZE * TILE_SIZE + TILE_SIZE / 2,
        }
      end
    when SF::Event::Resized
      factor = view.size.x.to_f / game.size.x
      view.size = SF.vector2i(event.width, event.height)
      view.zoom factor
    end
  end

  def draw
    game.view = game.default_view
    game.draw(background)
    game.view = view
    game.draw(map.tile_map)
    game.draw(ship)
    game.draw(selector)
  end

  private def init_map_planets
    (MAP_WIDTH * MAP_HEIGHT / 5).times do
      planet = Planet.new
      while true
        map_coords = SF.vector2i(rand(MAP_WIDTH), rand(MAP_HEIGHT))
        break unless map.get(map_coords)
      end
      map.set map_coords, planet
    end
    map.generate_tile_map
  end

  class View < SF::View
    def move(direction : Direction)
      movement =
        size / 50 *
          case direction
          when Direction::Up
            SF.vector2(0, -1)
          when Direction::Down
            SF.vector2(0, 1)
          when Direction::Left
            SF.vector2(-1, 0)
          when Direction::Right
            SF.vector2(1, 0)
          else
            SF.vector2(0, 0)
          end
      super movement
    end

    def zoom_in
      zoom(1 / 1.1)
    end

    def zoom_out
      zoom(1.1)
    end
  end
end
