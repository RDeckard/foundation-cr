class Planet < SGE::Entity
  RESOURCES = [:gold, :iron]
  {% for symbol in RESOURCES %}
    getter {{symbol.id}} = 0
  {% end %}

  def tile_index
    rand(18)
  end
end
