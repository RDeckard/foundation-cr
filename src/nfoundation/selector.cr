class Selector < SGE::Entity
  include Movable

  def initialize
    super
    render_texture = SF::RenderTexture.new(300, 300)
    render_texture.clear SF.color(255, 255, 255, 64)
    self.texture = render_texture.texture
    self.origin = center
  end

  def move_step
    DemoScene::TILE_SIZE
  end
end
