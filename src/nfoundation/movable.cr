module Movable
  alias Direction = Game::Direction

  def move(direction : Direction)
    movement =
      SF.vector2(move_step, move_step) *
        case direction
        when Direction::Up
          SF.vector2(0, -1)
        when Direction::Down
          SF.vector2(0, 1)
        when Direction::Left
          SF.vector2(-1, 0)
        when Direction::Right
          SF.vector2(1, 0)
        else
          SF.vector2(0, 0)
        end
    super movement
  end

  def move_step
    raise "#move_step must be defined when Movable is included"
  end
end
