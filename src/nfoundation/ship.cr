class Ship < SGE::Entity
  include Movable

  def move_step
    DemoScene::TILE_SIZE / 6
  end
end
